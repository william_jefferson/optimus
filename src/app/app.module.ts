import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';
import {AppComponent} from './app.component';
import { HttpClientModule } from '@angular/common/http';
import {CalendarWeekHoursViewModule} from './modules/calendar-week-hours-view/calendar-week-hours-view.module';
import { CalendarModule, CalendarNativeDateFormatter, DateFormatterParams, CalendarDateFormatter } from 'angular-calendar';
import {CustomDateFormatter} from './dateFormat';
import { FormsModule } from '@angular/forms';
@NgModule({
    declarations: [
        AppComponent
    ],
    imports: [
      HttpClientModule,
        BrowserModule,
        FormsModule,
        NgbModule.forRoot(),
        CalendarModule.forRoot({
         dateFormatter: {
           provide: CalendarDateFormatter,
           useClass: CustomDateFormatter
         }
       }),
        CalendarWeekHoursViewModule
    ],
    providers: [],
    bootstrap: [AppComponent]
})
export class AppModule {
}
