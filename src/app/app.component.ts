import {Component, OnInit} from '@angular/core';
import {addDays, addHours, endOfDay, endOfMonth, isSameDay, isSameMonth, startOfDay, subDays} from 'date-fns';
import {Subject} from 'rxjs/Subject';
import {CalendarEvent, CalendarEventTimesChangedEvent} from 'angular-calendar';
import { HttpClient, HttpParams } from '@angular/common/http';
import {NgbDateStruct, NgbCalendar} from '@ng-bootstrap/ng-bootstrap';
import { map } from 'rxjs/operators';
import { Observable } from 'rxjs';
const colors: any = {
    red: {
        primary: '#ad2121',
        secondary: '#FAE3E3'
    },
    blue: {
        primary: '#1e90ff',
        secondary: '#D1E8FF'
    },
    yellow: {
        primary: '#fff',
        secondary: '#fff'
    }
};

@Component({
    selector: 'app-root',
    templateUrl: './app.component.html',
    styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {

    view = 'week';
    startDay: number;
    model: NgbDateStruct;
    viewDate: Date = new Date();
    refresh: Subject<any> = new Subject();

    events: Observable<Array<CalendarEvent<{ film: any[] }>>>;

    constructor( private http: HttpClient) {}

    ngOnInit(): void {
     this.fetchEvents();
     this.startDay = this.viewDate.getDay();

    }

    selectDay(day){
      this.viewDate = new Date(day.year, day.month, day.day);
      this.ngOnInit();
    }

    eventClicked(event) {
        console.log(event);
        window.alert( 'Details' + '' + JSON.stringify(event));
    }

    hourSegmentClicked(event) {
        console.log(event);
    }

      fetchEvents(): void {

        const params = new HttpParams()
        this.events = this.http
          .get('https://daq80z2exg.execute-api.ap-southeast-1.amazonaws.com/prod/appointments/getByDoctor/5b012046cd57ec000144e08f')
          .pipe(
            map(({ message }: { message: any[] }) => {
              return message.map((film: any) => {
                return {
                  title: film.status + ' ' ,
                  start: new Date(film.from),
                  end: new Date(film.to),
                  color: colors.yellow,
                  meta: {
                    film
                  }
                };
              });
            })
          );
      }

    eventTimesChanged({
                          event,
                          newStart,
                          newEnd
                      }: CalendarEventTimesChangedEvent): void {
        event.start = newStart;
        event.end = newEnd;
        this.refresh.next();
    }

}
